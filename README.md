Extensions for Dependent Object Types
-------------------------------------

The DOT (Dependent Object Types) calculus by [Amin et al. (2016)](http://infoscience.epfl.ch/record/215280/files/paper_1.pdf) aims to formalizes Scala, specifically, abstract type members and path-dependent types.

This different folders of this repository contains extensions to DOT that aim to bridge the gap between DOT and Scala, and to experiment with new Scala features.

The extensions are based on the [simple](https://github.com/amaurremi/dot-calculus/tree/master/src/simple-proof) type-safety proof, which we started as a fork of the [original](https://github.com/samuelgruetter/dot-calculus) proof as presented by Amin et al. (2016).

If you want to understand the DOT safety proof, or are interested in creating your own extensions to DOT, you can read our [OOPSLA](https://plg.uwaterloo.ca/~olhotak/pubs/oopsla17.pdf) paper, and check out the corresponding [Coq](https://github.com/amaurremi/dot-calculus/tree/master/src/simple-proof) proof.

## Extensions

We currently provide the following extensions:

- [κDOT](https://git.uwaterloo.ca/ikabir/dot-public/tree/master/kDOT)
  ([paper](http://doi.acm.org/10.1145/3241653.3241659))
  an extension of DOT with field mutation using bounded field types and dependent constructors for object creation
