(** remove printing ~ *)

Set Implicit Arguments.

Require Import Coq.Program.Equality.
Require Import LibExtra DotTactics.
Require Import AbstractSyntax
        GeneralTyping RecordAndInertTypes PreciseTyping TightTyping
        InvertibleTyping GeneralToTight InertTightSubtyping
        Weakening Substitution.

Hint Resolve general_to_tight_typing : core.

Lemma inert_trm_con_typing : forall G Ts T Ts' T' ds t,
    inert G ->
    G ⊢ lit_con Ts' T' ds t ∶ typ_con Ts T ->
    Ts' = Ts /\ T' = T.
Proof.
  introv Hi HT.
  apply inert_con_typing in HT; inversions HT; auto.
Qed.

Lemma inert_trm_con_defs : forall G Ts T ds t,
    inert G ->
    G ⊢ lit_con Ts T ds t ∶ typ_con Ts T ->
    exists L, forall (x : var) (ys : list var),
        length ys = length_s Ts ->
        fresh L (S (length ys)) (x :: ys) ->
        G & ys ~** to_list Ts & x ~ open_vars (x :: ys) T ⊢
        open_vars (x :: ys) ds ∶ open_vars (x :: ys) T.
Proof.
  introv Hi Ht. inversions Ht; eauto.
Qed.

Lemma inert_trm_con_trm : forall G Ts T ds t,
    inert G ->
    G ⊢ lit_con Ts T ds t ∶ typ_con Ts T ->
    exists L T', forall (x : var) (ys : list var),
        length ys = length_s Ts ->
        fresh L (S (length ys)) (x :: ys) ->
        (G & ys ~** to_list Ts & x ~ open_vars (x :: ys) T)
          ⊢ open_vars (x :: ys) t ∶ open_vars (x :: ys) T'.
Proof.
  introv Hi Ht. inversions Ht; eauto.
Qed.

Lemma inert_trm_con_apply_trm : forall G Ts T ds t x xs avs,
    inert G ->
    G ⊢ lit_con Ts T ds t ∶ typ_con Ts T ->
    x # G ->
    x \notin fv (open_rec_vars 1 xs ds) ->
    length xs = length_s Ts ->
    vars_of_avars xs avs ->
    G ⊢ avs :: Ts ->
    exists T', G & x ~ open_rec_vars 1 xs (open x T)
                ⊢  open_rec_vars 1 xs (open x t) ∶ T'.
Proof.
  intros.
  apply (inert_trm_con_trm H) in H0. destruct H0 as [L [T' Htrm]].
  remember (L \u fv T \u fv Ts
              \u fv t \u fv T' \u fv_env G
              \u dom G \u \{ x}) as L' eqn:Heq.
  assert (exists x' ys', fresh L' (S (length ys')) (x' :: ys')
                    /\ length ys' = length_s Ts).
  { pose proof (var_freshes L' (S (length_s Ts))).
    destruct H0 as [xs' Fr]. pose proof (fresh_length _ _ _ Fr) as Hlen.
    destruct xs' as [| x' xs']; inversions Hlen.
    exists x' xs'. auto. }
  destruct H0 as [x' [ys' [?H ?H]]].
  assert (fresh L (S (length ys')) (x' :: ys')) by (subst L'; auto).
  specialize (Htrm _ _ H6 H7). clear H7.
  intros. exists (open_vars (x :: xs) T').


  assert (G & x' ~ open_vars (x' :: xs) T ⊢
          open_vars (x' :: xs) t ∶ open_vars (x' :: xs) T').
  { sympl. sympl in Htrm. destruct H0.
    eapply (weaken_middle_singles_trm ys') with (L:=L); subst; eauto 3.
    pose proof (fv_open_cases x' T 0) as Hfv1.
    pose proof (fv_open_cases x' t 0) as Hfv2.
    pose proof (fv_open_cases x' T' 0) as Hfv3.
    destruct Hfv1 as [Hfv1 | Hfv1]; destruct Hfv2 as [Hfv2 | Hfv2];
      destruct Hfv3 as [Hfv3 | Hfv3];
      rewrite Hfv1; rewrite Hfv2; rewrite Hfv3; auto. }

  assert (ok (G & x ~ open_rec_vars 1 xs (open x T))) by auto.
  assert (x' # (G & x ~ open x (open_rec_vars 1 xs T)))
    by (destruct H0; subst L'; auto).
  eapply weaken_rules in H7; try reflexivity; try eassumption.

  sympl. rewrite ? open_vars_S_commut.
  eapply renaming_open_typ; eauto 2.
  + assert (x' # G) by auto. subst L'. destruct H0.
    assert (x' \notin fv (open_rec_vars 1 xs t)).
    { apply notin_open_rec_vars_list; auto.
      apply (notin_vars_of_avars H10 H5 H4). }
    assert (x' \notin fv (open_rec_vars 1 xs T)).
    { apply notin_open_rec_vars_list; auto.
      apply (notin_vars_of_avars H10 H5 H4). }
    assert (x' \notin fv (open x (open_rec_vars 1 xs T))).
    { destruct (fv_open_cases x (open_rec_vars 1 xs T) 0)
        as [H' | H']; rewrite H'; auto. }
    assert (x' \notin fv (open_rec_vars 1 xs T')).
    { apply notin_open_rec_vars_list; auto.
      apply (notin_vars_of_avars H10 H5 H4). }
    assert (x' \notin fv (open x (open_rec_vars 1 xs T'))).
    { destruct (fv_open_cases x (open_rec_vars 1 xs T') 0)
        as [H' | H']; rewrite H'; auto. }
    rewrite fv_in_values_concat, fv_env_single. notin_solve.
  + sympl in H6. repeat rewrite <- open_vars_S_commut.
    auto.
Qed.

Lemma inert_trm_con_apply_defs : forall G Ts T ds t x xs avs,
    inert G ->
    G ⊢ lit_con Ts T ds t ∶ typ_con Ts T ->
    x # G ->
    x \notin fv (open_rec_vars 1 xs ds) ->
    length xs = length_s Ts ->
    vars_of_avars xs avs ->
    G ⊢ avs :: Ts ->
    G & x ~ open_rec_vars 1 xs (open x T)
          ⊢ open_rec_vars 1 xs (open x ds)
          ∶ open_rec_vars 1 xs (open x T).
Proof.
  intros.
  apply (inert_trm_con_defs H) in H0. destruct H0 as [L Hdefs].
  remember (L \u fv T \u fv Ts
              \u fv ds \u fv_env G
              \u dom G \u \{ x}) as L' eqn:Heq.
  assert (exists x' ys', fresh L' (S (length ys')) (x' :: ys')
                    /\ length ys' = length_s Ts).
  { pose proof (var_freshes L' (S (length_s Ts))).
    destruct H0 as [xs' Fr]. pose proof (fresh_length _ _ _ Fr) as Hlen.
    destruct xs' as [| x' xs']; inversions Hlen.
    exists x' xs'. auto. }
  destruct H0 as [x' [ys' [?H ?H]]].
  assert (fresh L (S (length ys')) (x' :: ys')) by (subst L'; auto).
  specialize (Hdefs _ _ H6 H7). clear H7.
  assert (G & x' ~ open_vars (x' :: xs) T ⊢
          open_vars (x' :: xs) ds ∶ open_vars (x' :: xs) T).
  { sympl. destruct H0.
    eapply (weaken_middle_singles_defs) with (L:=L); subst; eauto 3.
    pose proof (fv_open_cases x' T 0) as Hfv1.
    pose proof (fv_open_cases x' ds 0) as Hfv2.
    destruct Hfv1 as [Hfv1 | Hfv1]; destruct Hfv2 as [Hfv2 | Hfv2];
      rewrite Hfv1; rewrite Hfv2; auto. }
  assert (ok (G & x ~ open_rec_vars 1 xs (open x T))) by auto.
  assert (x' # (G & x ~ open x (open_rec_vars 1 xs T)))
    by (destruct H0; subst L'; auto).
  eapply weaken_rules in H7; try reflexivity; try eassumption.
  repeat rewrite open_vars_S_commut.
  eapply renaming_def; eauto 2.
  + assert (x' # G) by auto.
    assert (x' \notin fv (open_rec_vars 1 xs ds)).
    { subst L'; destruct H0.
      apply notin_open_rec_vars_list; auto.
      apply (notin_vars_of_avars H10 H5 H4). }
    assert (x' \notin fv (open_rec_vars 1 xs T)).
    { subst L'; destruct H0.
      apply notin_open_rec_vars_list; auto.
      apply (notin_vars_of_avars H10 H5 H4). }
    assert (x' \notin fv (open x (open_rec_vars 1 xs T))).
    { destruct (fv_open_cases x (open_rec_vars 1 xs T) 0)
        as [H' | H']; rewrite H'; subst L'; auto. }
    rewrite fv_in_values_concat, fv_env_single.
    subst L'; destruct H0. notin_solve.
  + repeat rewrite <- open_vars_S_commut.
    auto.
Qed.

Lemma inert_trm_con_record_type : forall G Ts T ds t,
    inert G ->
    G ⊢ lit_con Ts T ds t ∶ typ_con Ts T ->
    record_type T.
Proof.
  introv Hi Ht. pose proof (inert_trm_con_defs Hi Ht) as [L H].
  assert (Fr: exists x ys, fresh (L \u fv T) (S (length_s Ts)) (cons x ys)).
  { destruct (var_freshes (L \u fv T) (S (length_s Ts))) as [xys ?Fr].
    assert (length xys = S (length_s Ts)) by eauto.
    destruct xys; simpls; try congruence. exists v xys; auto. }
  destruct Fr as [x [ys Fr]].
  assert (Hlen: length ys = length_s Ts) by eauto.
  rewrite <- Hlen in Fr.
  assert (Fr': fresh L (S (length ys)) (x :: ys)) by auto.
  specialize (H _ _ Hlen Fr'). pose proof (ty_defs_record_type H).
  eapply record_type_open_rec_vars; eauto; auto.
Qed.
