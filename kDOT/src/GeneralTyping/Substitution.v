(** remove printing ~ *)

Set Implicit Arguments.

Require Import DotTactics LibExtra.
Require Import AbstractSyntax GeneralTyping Weakening.

Ltac unfold_fvar :=
  unfold subst_fvar; rewrite~ If_r.

Ltac subst_open_fresh :=
  repeat match goal with
    | [ |- context [ (?O ?n ?z (?S ?x ?y ?T)) ] ] =>
        (replace (O n z (S x y T)) with (O n (subst_fvar x y z) (S x y T))
          by unfold_fvar);
        let H := fresh in
        pose proof (subst_open_commut x y z T) as H;
        unfold subst_fvar; rewrite <- H; clear H
         end;
  try unfold_fvar.

Ltac fold_subst_env :=
  try rewrite ? subst_singles_rev_to_list by auto;
  try rewrite ? subst_single;
  rewrite <- ? concat_assoc, <- ? map_concat, ? concat_assoc.

Ltac unfold_subst_env :=
  rewrite map_concat, <- subst_single.

Ltac subst_solver :=
    fresh_constructor;
    subst_open_fresh;
    match goal with
    | [ H: forall z, z \notin ?L -> forall G, _
        |- context [ ?G1 & map (?S1 ?x ?y) ?G2 & ?z ~ ?S2 ?x ?y ?T] ] =>
      assert (G1 & map (S1 x y) G2 & z ~ S2 x y T
              = G1 & subst_env x y (G2 & z ~ T))
        as B by (unfold_subst_env; rewrite concat_assoc; auto);
      rewrite B; apply H; rewrite ? concat_assoc; auto 2;
      rewrite map_concat; rewrite ? concat_assoc;
      auto using weaken_ty_trm
    end.

Ltac subst_open_commut_solver :=
  sympls;
  try rewrite subst_open_commut;
  auto; eauto 4.


Ltac fold_subst :=
  repeat match goal with
    | [ |- context [ trm_var (avar_f (If ?x = ?y then ?z else ?x)) ] ] =>
        asserts_rewrite (trm_var (avar_f (If x = y then z else x))
                         = subst_trm y z (trm_var (avar_f x))); auto
    | [ |- context [ open (If ?x = ?y then ?z else ?x) (subst_var ?y ?z ?T) ] ] =>
        asserts_rewrite (open (If x = y then z else x) (subst_var y z T)
                     = subst_var y z (open x T)); auto  end.

Local Ltac simpl_length_s :=
  repeat match goal with
  | [ |- context[ length_s (_ /[ _ -> _]) ] ] =>
    rewrite length_s_subst
  | [ H: context[ length_s (_ /[ _ -> _]) ] |- _ ] =>
    rewrite length_s_subst in H
  end.


(** * Substitution Lemma *)
(** [G1, x: S, G2 ⊢ t∶ T]            #<br>#
    [ok(G1, x: S, G2)]               #<br>#
    [x \notin fv(G1)]                 #<br>#
    [G1, G2[y/x] ⊢ y∶ S[y/x]]       #<br>#
    [―――――――――――――――――――――――――――――]  #<br>#
    [G1, G2[y/x] ⊢ t[y/x]∶ T[y/x]] #<br>#  #<br>#

    and

    [G1, x: S, G2 ⊢ d∶ D]            #<br>#
    [ok(G1, x: S, G2)]               #<br>#
    [x \notin fv(G1)]                 #<br>#
    [G1, G2[y/x] ⊢ y∶ S[y/x]]       #<br>#
    [―――――――――――――――――――――――――――――]  #<br>#
    [G1, G2[y/x] ⊢ d[y/x]∶ D[y/x]] #<br>#  #<br>#

    and

    [G1, x: S, G2 ⊢ ds∶ T]           #<br>#
    [ok(G1, x: S, G2)]               #<br>#
    [x \notin fv(G1)]                 #<br>#
    [G1, G2[y/x] ⊢ y∶ S[y/x]]       #<br>#
    [――――――――――――――――――――――――――――――] #<br>#
    [G1, G2[y/x] ⊢ ds[y/x]∶ T[y/x]] #<br>#  #<br>#

    and

    [G1, x: S, G2 ⊢ T <: U]           #<br>#
    [ok(G1, x: S, G2)]                #<br>#
    [x \notin fv(G1)]                  #<br>#
    [G1, G2[y/x] ⊢ y∶ S[y/x]]        #<br>#
    [―――――――――――――――――――――――――――――――] #<br>#
    [G1, G2[y/x] ⊢ T[y/x] <: U[y/x]] #<br>#  #<br># *)

(** The proof is by mutual induction on term typing, definition typing, and subtyping. *)
Lemma subst_rules: forall y S,
  (forall G t T, G ⊢ t ∶ T -> forall G1 G2 x,
    G = G1 & x ~ S & G2 ->
    ok (G1 & x ~ S & G2) ->
    x \notin fv_env G1 ->
    G1 & (subst_env x y G2) ⊢ trm_var (avar_f y) ∶ subst_var x y S ->
    G1 & (subst_env x y G2) ⊢ subst_var x y t ∶ subst_var x y T) /\
  (forall G (l : lit) T, G ⊢ l ∶ T -> forall G1 G2 x,
    G = G1 & x ~ S & G2 ->
    ok (G1 & x ~ S & G2) ->
    x \notin fv_env G1 ->
    G1 & (subst_env x y G2) ⊢ trm_var (avar_f y) ∶ subst_var x y S ->
    G1 & (subst_env x y G2) ⊢ subst_var x y l ∶ subst_var x y T) /\
  (forall G d D, G ⊢ d ∶d D -> forall G1 G2 x,
    G = G1 & x ~ S & G2 ->
    ok (G1 & x ~ S & G2) ->
    x \notin fv_env G1 ->
    G1 & (subst_env x y G2) ⊢ trm_var (avar_f y) ∶ subst_var x y S ->
    G1 & (subst_env x y G2) ⊢ subst_var x y d ∶d subst_var x y D) /\
  (forall G (ds : defs) T, G ⊢ ds ∶ T -> forall G1 G2 x,
    G = G1 & x ~ S & G2 ->
    ok (G1 & x ~ S & G2) ->
    x \notin fv_env G1 ->
    G1 & (subst_env x y G2) ⊢ trm_var (avar_f y) ∶ subst_var x y S ->
    G1 & (subst_env x y G2) ⊢ subst_var x y ds ∶ subst_var x y T) /\
  (forall G T U, G ⊢ T <: U -> forall G1 G2 x,
    G = G1 & x ~ S & G2 ->
    ok (G1 & x ~ S & G2) ->
    x \notin fv_env G1 ->
    G1 & (subst_env x y G2) ⊢ trm_var (avar_f y) ∶ subst_var x y S ->
    G1 & (subst_env x y G2) ⊢ subst_var x y T <: subst_var x y U) /\
  (forall G avs Ts, G ⊢ avs :: Ts -> forall G1 G2 x,
    G = G1 & x ~ S & G2 ->
    ok (G1 & x ~ S & G2) ->
    x \notin fv_env G1 ->
    G1 & (subst_env x y G2) ⊢ trm_var (avar_f y) ∶ subst_var x y S ->
    G1 & (subst_env x y G2) ⊢ subst_var x y avs :: subst_var x y Ts).
Proof.
  introv.
  apply rules_mutind; intros; subst; sympl in *;
    try subst_solver; subst_open_commut_solver.
  - Case "ty_var".
    cases_if.
    + apply binds_middle_eq_inv in b; subst; assumption.
    + apply (subst_fresh_ctx y) in H1.
      rewrite <- H1, <- map_concat.
      eauto using binds_map, binds_subst.
  - Case "ty_new".
    rewrite subst_open_rec_vars_commut.
    econstructor; auto 2. apply subst_vars_of_avars; auto.
  - Case "ty_rec_intro".
    apply ty_rec_intro. fold_subst; sympl; eauto.
    rewrite subst_open_commut. auto.
  - Case "ty_con_intro".
    clear t0 t1.
    apply* (@ty_con_intro (L \u dom (G1 & x ~ S & G2))); intros.
    + repeat rewrite <- subst_open_vars_commut_fresh_length
        by (simpls; destruct_all; simpl_dom; auto).
      simpl_length_s; fold_subst_env.
      destruct H5. apply H; rewrite ? concat_assoc; auto.
      rewrite <- ? concat_assoc. rewrite map_concat.
      rewrite concat_assoc. apply weaken_ty_trm; auto.
      rewrite <- concat_assoc. rewrite <- map_concat.
      apply ok_concat_map. rewrite ? concat_assoc.
      apply fresh_ok; auto 3. simpl_dom; auto.
    + repeat rewrite <- subst_open_vars_commut_fresh_length
        by (simpls; destruct_all; simpl_dom; split; auto).
      simpl_length_s; fold_subst_env.
      destruct H5. sympl.
      assert (fresh L (length (x0 :: ys)) (x0 :: ys)) by auto.
      specialize (H0 x0 ys H1 H7 G1
                     (G2
                      & ys ~** to_list Ts
                      & x0 ~ open_rec_vars 1 ys (open x0 T)) x).
      assert ((open_rec_vars 1 ys (open x0 T') /[ x -> y])
                = (open_rec_vars 1 ys (open x0 (T' /[ x -> y])))).
      { rewrite subst_open_vars_commut_fresh_length
          by (simpl_dom; auto).
        assert (x0 <> x) by (rewrite <- notin_singleton; simpl_dom; auto).
        rewrite subst_open_commut; cases_if; auto. }
      rewrite H8 in H0.
      apply H0; rewrite ? concat_assoc; auto.
      rewrite <- ? concat_assoc. rewrite map_concat.
      rewrite concat_assoc. apply weaken_ty_trm; auto.
      rewrite <- concat_assoc. rewrite <- map_concat.
      apply ok_concat_map. rewrite ? concat_assoc.
      apply fresh_ok; auto 3. simpl_dom; auto.
  - Case "ty_defs_cons".
    constructor*. rewrite <- subst_label_of_def. apply* subst_defs_hasnt.
Qed.

(** The substitution lemma for term typing. *)
Lemma subst_ty_trm: forall y S G x (t : trm) T,
    G & x ~ S ⊢ t ∶ T ->
    ok (G & x ~ S) ->
    x \notin fv_env G ->
    G ⊢ trm_var (avar_f y) ∶ subst_var x y S ->
    G ⊢ subst_var x y t ∶ subst_var x y T.
Proof.
  intros.
  apply (proj51 (subst_rules y S)) with (G1:=G) (G2:=empty) (x:=x) in H;
  try rewrite map_empty in *; try rewrite concat_empty_r in *; auto.
Qed.

(** The substitution lemma for definition typing. *)
Lemma subst_ty_defs: forall y S G x (ds : defs) T,
    G & x ~ S ⊢ ds ∶ T ->
    ok (G & x ~ S) ->
    x \notin fv_env G ->
    G ⊢ trm_var (avar_f y) ∶ subst_var x y S ->
    G ⊢ subst_var x y ds ∶ subst_var x y T.
Proof.
  intros.
  apply (subst_rules y S) with (G1:=G) (G2:=empty) (x:=x) in H;
    try rewrite map_empty in *; try rewrite concat_empty_r in *; auto.
Qed.

Lemma subst_ctx_vars_ok : forall xs ys G1 G2,
    ok (G1 & G2) ->
    ok (G1 & subst_env_vars xs ys G2).
Proof.
  intros. apply ok_concat_map; auto.
Qed.

(** * Renaming  *)

(** Renaming the name of the opening variable for definition typing.  #<br>#

    [ok G]                   #<br>#
    [z] fresh                #<br>#
    [G, z: T^z ⊢ ds^z ∶ T^z] #<br>#
    [G ⊢ x∶ T^x]             #<br>#
    [――――――――――――――――――――――] #<br>#
    [G ⊢ ds^x ∶ T^x]         *)
Lemma renaming_def : forall G n z T (ds : defs) x,
    ok G ->
    z # G ->
    z \notin (fv_env G \u fv ds \u fv T) ->
    G & z ~ open z T ⊢ open_rec n z ds ∶ open_rec n z T ->
    G ⊢ trm_var (avar_f x) ∶ open x T ->
    G ⊢ open_rec n x ds ∶ open_rec n x T.
Proof.
  introv Hok Hnz Hnz' Hz Hx.
  rewrite subst_intro with (x0:=z) (y:=x) by auto.
  rewrite subst_intro with (x0:=z) (y:=x) by auto.
  eapply subst_ty_defs; auto. eapply Hz. rewrite <- subst_intro. all: auto.
Qed.

(** Renaming the name of the opening variable for term typing. #<br>#
    [ok G]                   #<br>#
    [z] fresh                #<br>#
    [G, z: U ⊢ t^z ∶ T^z]    #<br>#
    [G ⊢ x∶ U]               #<br>#
    [――――――――――――――――――――――] #<br>#
    [G ⊢ t^x ∶ T^x]         *)
Lemma renaming_typ: forall G z T U (t : trm) x,
    ok G ->
    z # G ->
    z \notin (fv_env G \u fv U \u fv T \u fv t) ->
    G & z ~ U ⊢ open z t ∶ open z T ->
    G ⊢ trm_var (avar_f x) ∶ U ->
    G ⊢ open x t ∶ open x T.
Proof.
  introv Hok Hnz Hnz' Hz Hx.
  rewrite subst_intro with (x0:=z) (y:=x).
  rewrite subst_intro with (x0:=z) (y:=x).
  eapply subst_ty_trm; auto. eapply Hz. rewrite subst_fresh. all: auto.
Qed.

Lemma renaming_open_typ: forall G z T U (t : trm) x,
    ok G ->
    z # G ->
    z \notin (fv_env G \u fv U \u fv T \u fv t) ->
    G & z ~ open z U ⊢ open z t ∶ open z T ->
    G ⊢ trm_var (avar_f x) ∶ open x U ->
    G ⊢ open x t ∶ open x T.
Proof.
  introv Hok Hnz Hnz' Hz Hx.
  rewrite subst_intro with (x0:=z) (y:=x).
  rewrite subst_intro with (x0:=z) (y:=x).
  eapply subst_ty_trm; auto. eapply Hz.
  rewrite <- subst_intro. all: auto.
Qed.

(** Renaming the name of the opening variable for term typing. #<br>#
    [ok G]                   #<br>#
    [z] fresh                #<br>#
    [G, z: U ⊢ t^z ∶ T^z]    #<br>#
    [――――――――――――――――――――――] #<br>#
    [G ⊢ t^x ∶ T^x]         *)
Lemma renaming_fresh : forall L G T (t : trm) U x,
    ok G ->
    (forall x : var, x \notin L -> G & x ~ T ⊢ open x t ∶ U) ->
    G ⊢ trm_var (avar_f x) ∶ T ->
    G ⊢ open x t ∶ U.
Proof.
  introv Hok Hu Hx.
  pick_fresh y.
  rewrite subst_intro with (x0:=y) (y0:=x) by auto.
  rewrite <- subst_fresh with (x0:=y) (y0:=x) by auto.
  eapply subst_ty_trm; auto. rewrite subst_fresh; auto.
Qed.

Lemma renaming_push : forall L G T (t : trm) U x,
    ok (G & x ~ T) ->
    (forall x : var, x \notin L -> G & x ~ T ⊢ open x t ∶ U) ->
    G & x ~ T ⊢ open x t ∶ U.
Proof.
  introv Hok Hu. eapply renaming_fresh with (L:=L); eauto.
  intros. eapply weaken_rules; eauto.
Qed.

Lemma weaken_middle_trm : forall y' n T' G T T'' (t : trm) x x',
    ok (G & x ~ open_rec n x' T) ->
    y' \notin (fv T \u fv T' \u fv T'' \u fv t \u fv_env G \u dom G \u \{x}) ->
    G & y' ~ T' & x ~ open_rec n y' T ⊢ open_rec n y' t ∶ open_rec n y' T'' ->
    G ⊢ trm_var (avar_f x') ∶ T' ->
    G & x ~ open_rec n x' T ⊢ open_rec n x' t ∶ open_rec n x' T''.
Proof.
  intros.
  repeat rewrite (subst_intro (x:=y') x' n); auto.
  rewrite subst_single.
  apply ok_push_inv in H. destruct H.
  eapply (proj51 (subst_rules x' T')); trivial; try assumption; auto 3.
  rewrite subst_fresh; auto. rewrite <- subst_single. apply weaken_ty_trm; auto.
Qed.


Lemma subst_middle_singles_trm : forall G x x0 x1 n ys T Ts (t : trm) U T',
  ok G ->
  x0 # G ->
  length ys = length_s Ts ->
  fresh
         (fv T \u
          fv Ts \u fv U \u
          fv t \u fv T' \u fv_env G \u dom G \u \{ x0})
         (length (x :: ys)) (x :: ys) ->
  G & ys ~** to_list Ts & x ~ U &
       x0 ~ open_rec (n + length ys) x (open_rec_vars n ys T)
       ⊢ open_rec (n + length ys) x (open_rec_vars n ys t)
       ∶ open_rec (n + length ys) x (open_rec_vars n ys T') ->
  G ⊢ trm_var (avar_f x1) ∶ U ->
  G & ys ~** to_list Ts &
       x0 ~ open_rec (n + length ys) x1 (open_rec_vars n ys T)
       ⊢ open_rec (n + length ys) x1 (open_rec_vars n ys t)
       ∶ open_rec (n + length ys) x1 (open_rec_vars n ys T').
Proof.
  intros.
  apply (@weaken_middle_trm x _ U);
    eauto using fresh_ok_rev, weaken_ty_trm, fresh_ok_rev_ys.
  rewrite fv_in_values_concat, fv_env_singles, <- ? union_assoc by auto.
  rewrite dom_concat, dom_singles by (rewrite liblist_length; auto).
  notin_solve; try apply notin_open_rec_vars_list;
    auto 2 using from_list_fresh.
Qed.


Lemma weaken_middle_singles_trm : forall ys n Ts L G T T' (t : trm) x avs xs,
    ok (G & x ~ open_rec_vars n xs T) ->
    length ys = length_s Ts ->
    fresh (L \u fv T \u fv Ts
             \u fv t \u fv T'
             \u fv_env G \u dom G \u \{x}) (length ys) ys ->
    G & (ys ~** to_list Ts) & x ~ open_rec_vars n ys T ⊢
                      open_rec_vars n ys t ∶ open_rec_vars n ys T' ->
    G ⊢ avs :: Ts ->
    vars_of_avars xs avs ->
    G & x ~ open_rec_vars n xs T ⊢ open_rec_vars n xs t ∶ open_rec_vars n xs T'.
Proof.
  induction ys using List.rev_ind.
  - intros. destruct Ts; inversion H0.
    simpl in H2. rewrite singles_nil, concat_empty_r in H2.
    inversions H3. inversions H4. auto.
  - intros n Ts. gen n. induction Ts using listlike_rev_ind.
    intros. rewrite List.app_length, Nat.add_1_r in H0. inversion H0.
    clear IHTs. intros.
    (* simplify length  eq *)
    rewrite List.app_length, app_s_length_s, ? Nat.add_1_r in H0. inversion H0.
    (* simplify H2 context *)
    sympl in H2.
    rewrite singles_rev_cons, concat_assoc in H2.
    (* simplify H2 typing *)
    repeat rewrite open_vars_app in H2. sympl in H2.
    repeat rewrite open_rec_vars_open_rec_commut in H2 by omega.
    (* *)
    apply fresh_app in H1; simpl List.app in H1.
    (* split up avar typing in H3 *)
    apply ty_avars_concat in H3.
    destruct H3 as [avs1 [avs2 [?H [?H ?H]]]]; subst.
    inversions H7. inversions H12.
    (* split vars of avars typing in H4 *)
    apply vars_of_avars_app_r in H4.
    destruct H4 as [ys' [zs' [?H [?H ?H]]]]; subst.
    inversions H7. inversions H9.
    (* weaken H2 *)
    clear H0. rename H6 into H0.
    apply ok_push_inv in H as [? ?].
    simpl of_list in H1; simpl app_s in H1.
    rewrite of_list_app, fv_app_union,
    to_list_of_list, fv_one, <- ? union_assoc in H1.
    apply (subst_middle_singles_trm (x1:=x1)) in H2; auto 2.

    (* Proving goal *)
    repeat rewrite open_vars_app. sympl.
    eapply IHys with (L:=L); eauto 2.
    + sympl in H1.
      destruct H1. fresh_solve; eauto 3.
      * destruct (fv_open_cases x1 T (n + length ys')); rewrite H7; auto.
        apply typing_implies_bound in H11. destruct H11 as [?S H11].
        apply binds_to_dom in H11.
        fresh_solve. eauto using fresh_single_in.
      * destruct (fv_open_cases x1 t (n + length ys')); rewrite H7; auto.
        apply typing_implies_bound in H11. destruct H11 as [?S H11].
        apply binds_to_dom in H11.
        fresh_solve. eauto using fresh_single_in.
      * destruct (fv_open_cases x1 T' (n + length ys')); rewrite H7; auto.
        apply typing_implies_bound in H11. destruct H11 as [?S H11].
        apply binds_to_dom in H11.
        fresh_solve. eauto using fresh_single_in.
    + assert (length ys = length ys').
      { apply length_vars_of_avars in H4.
        apply length_ty_avars in H5. omega. }
      repeat rewrite open_rec_vars_open_rec_commut by omega.
      rewrite <- H6; auto.
Qed.

Lemma weaken_middle_defs : forall y' n T' L G T (ds : defs) x x',
    ok (G & x ~ open_rec n x' T) ->
    y' \notin (L \u fv T \u fv T' \u fv ds \u fv_env G \u dom G \u \{x}) ->
    G & y' ~ T' & x ~ open_rec n y' T ⊢ open_rec n y' ds ∶ open_rec n y' T ->
    G ⊢ trm_var (avar_f x') ∶ T' ->
    G & x ~ open_rec n x' T ⊢ open_rec n x' ds ∶ open_rec n x' T.
Proof.
  intros.
  repeat rewrite (subst_intro (x:=y') x' n); auto.
  rewrite subst_single.
  apply ok_push_inv in H. destruct H.
  eapply (proj54 (subst_rules x' T')); trivial; try assumption; auto 3.
  rewrite subst_fresh; auto. rewrite <- subst_single. apply weaken_ty_trm; auto.
Qed.

Lemma weaken_middle_singles_defs : forall ys n Ts L G T (ds : defs) x avs xs,
    ok (G & x ~ open_rec_vars n xs T) ->
    length ys = length_s Ts ->
    fresh (L \u fv T \u fv Ts
             \u fv ds
             \u fv_env G \u dom G \u \{x}) (length ys) ys ->
    G & (ys ~** to_list Ts) & x ~ open_rec_vars n ys T ⊢
                      open_rec_vars n ys ds ∶ open_rec_vars n ys T ->
    G ⊢ avs :: Ts ->
    vars_of_avars xs avs ->
    G & x ~ open_rec_vars n xs T ⊢ open_rec_vars n xs ds ∶ open_rec_vars n xs T.
Proof.
  induction ys using List.rev_ind; intros.
  - unfold length_s in H0. destruct Ts; inversion H0.
    inversions H3. inversion H4. simpl in *.
    rewrite singles_nil, concat_empty_r in H2; auto.
  - unfold length_s in H0. rewrite List.app_length in H0.
    assert (R: Ts = (of_list (to_list Ts)))
      by (symmetry; apply to_list_of_list); rewrite R in *; clear R.
    rewrite of_list_to_list in H0.
    rewrite Nat.add_comm in H0.
    induction (to_list Ts) using List.rev_ind.
    inversion H0. clear IHl. rename x1 into T'.
    rewrite of_list_app in H3. apply ty_avars_concat in H3.
    destruct H3 as [avs1 [avs2 H3]]. destruct_ands.
    inversions H6. inversions H12.
    apply vars_of_avars_app_r in H4.
    destruct H4 as [ys' [zs' H4]]; destruct_ands. subst.
    inversions H6. inversions H8.
    repeat rewrite open_vars_app in *. sympl.

    replace (S (length (ys ++ x :: nil)))
      with (length (x0 :: (ys ++ x :: nil)%list)) in H1 by auto.
    apply fresh_app in H1.
    rewrite of_list_app, fv_app_union, <- ? union_assoc in H1.
    assert ((fv (of_list (T' :: nil))) = fv T').
    { sympl. auto. }
    rewrite H3 in H1. clear H3.

    rewrite of_list_to_list in H2.
    rewrite ? List.rev_app_distr in H2.
    sympl in H2. rewrite singles_push_r, ? concat_assoc in H2.

    rewrite List.app_length, Nat.add_1_r in H0. inversion H0.
    apply (IHys n (of_list l) L _ _ _ _ avs1);
      try (unfold length_s; rewrite of_list_to_list); auto.
    + destruct H1 as [H1' H1]. rewrite List.app_nil_l in H1.
      repeat apply fresh_union_l; auto 2.
      * pose proof (fv_open_cases x1 T (n + length ys')).
        destruct H3; rewrite H3; auto.
        apply fresh_union_l; auto 2.
        apply typing_implies_bound in H11. destruct H11 as [?S H11].
        apply binds_to_dom in H11. clear S.
        eapply fresh_single_in; eauto.
      * pose proof (fv_open_cases x1 ds (n + length ys')).
        destruct H3; rewrite H3; auto.
        apply fresh_union_l; auto 2.
        apply typing_implies_bound in H11. destruct H11 as [?S H11].
        apply binds_to_dom in H11. clear S.
        eapply fresh_single_in; eauto.
    + assert (length ys' = length ys).
      { rewrite H6; clear H6. apply length_ty_avars in H5.
        unfold length_s in H5. rewrite (of_list_to_list l) in H5.
        rewrite <- H5; clear H5. apply length_vars_of_avars in H4; auto. }
      repeat rewrite open_rec_vars_open_rec_commut; try omega.
      apply (weaken_middle_defs) with (y':=x) (T':=T') (L:=L).
      * apply fresh_ok_rev; auto 2. split.
        -- apply ok_push_inv in H. apply H.
        -- destruct H1. rewrite List.app_nil_l in H7. auto.
      * destruct H1 as [H1' H1]. rewrite List.app_nil_l in H1.
        rewrite ? union_assoc.
        repeat (apply notin_union; split;
                try apply notin_open_rec_vars; auto 2;
                try apply fresh_union_l; auto 2).
        -- rewrite fv_in_values_concat. apply notin_union. split; auto.
           rewrite <- (of_list_to_list l).
           rewrite fv_env_singles; auto.
           unfold length_s. rewrite of_list_to_list; auto.
        -- apply fresh_cons_notin_rev; auto.
      * rewrite H3.
        repeat rewrite <- open_rec_vars_open_rec_commut; try omega.
        auto.
      * apply weaken_ty_trm; auto.
        eapply (@fresh_ok_rev_ys _ G _ x0); auto 2.
        split.
        -- apply ok_push_inv in H. apply H.
        -- destruct H1. rewrite List.app_nil_l in H7. auto.
Qed.
